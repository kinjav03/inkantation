//! Inkantation is a small CLI program that converts Kantu tests' source JSON files into ready-to-use Nightwatch tests.
mod conv;
use clap::{App, Arg};
use conv::*;
use serde_json;
use std::fs;
use std::path::*;
use toml;

fn main() {
    let inkantation = App::new("Inkantation")
        .version("v0.3")
        .author("Written by Javier K. <kinjav03@gmail.com>")
        .about("CLI tool for converting Kantu tests to Nightwatch tests.")
        .arg(
            Arg::with_name("INPUT")
                .help("Sets the input file or directory")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::with_name("OUTPUT")
                .help("Sets the output directory")
                .default_value(".")
                .index(2),
        )
        .get_matches();

    fn write_test(mut test: KantuTest, nightwatch_dict: &Dictionary, output: &str) {
        let test_string = &mut format!("\"{}\": function(browser) {{\nbrowser", test.Name);
        for i in 0..test.Commands.len() {
            convert_kantu_cmd(&mut test.Commands[i], test_string, &nightwatch_dict);
        }
        test_string.push_str("\n}");

        // Write the completed Nightwatch test to the specified directory
        let output_file = format!("{}/{}.js", output, test.Name);
        fs::write(output_file, test_string).expect("Failed to write output file");
    }

    // Read the passed JSON file to struct KantuTest for later usage
    let input_path = Path::new(inkantation.value_of("INPUT").unwrap());
    let output = inkantation.value_of("OUTPUT").unwrap();
    let dict_file = &fs::read_to_string("dict.toml").expect("Failed to find dict.toml");
    let nightwatch_dict: Dictionary = toml::from_str(dict_file).expect("Failed to read dict.toml");

    if input_path.is_dir(){
        let test_dir = input_path.read_dir().unwrap();
        for test_file in test_dir {
            println!("Converting test directory...");
            let contents = &fs::read_to_string(test_file.unwrap().path()).expect("Failed to read input file");
            let test: KantuTest = serde_json::from_str(contents).expect("Failed to parse JSON");
            write_test(test, &nightwatch_dict, output);
            println!("Conversion complete!");
        }
    } else {
        let contents = &fs::read_to_string(input_path).expect("Failed to read input file");
        let test: KantuTest = serde_json::from_str(contents).expect("Failed to parse JSON");
        println!(
            "Converting test {} created on date {}...",
            test.Name, test.CreationDate
        );
        write_test(test, &nightwatch_dict, output);
        println!("Conversion complete!");
    }
}
