use serde::Deserialize;

#[derive(Deserialize)]
pub struct KantuCommand {
    pub Command: String,
    pub Target: String,
    pub Value: String,
}

#[derive(Deserialize)]
pub struct KantuTest {
    pub Name: String,
    pub CreationDate: String,
    pub Commands: Vec<KantuCommand>,
}

// The Dictionary struct is a vector of DictEntry instances; individual entries in the dict.toml file.
// Each entry consists of the name of a Kantu command and the name of its Nightwatch equivalent.
#[derive(Deserialize, Clone)]
pub struct DictEntry {
    pub k_name: String,
    pub nw_name: String,
}

#[derive(Deserialize, Clone)]
pub struct Dictionary {
    pub entries: Vec<DictEntry>,
}

// Writes a Nightwatch command based on the input Kantu command and appends it to the current Nightwatch test string.
fn write_nwatch_cmd(kantu_cmd: &mut KantuCommand, nightwatch_test: &mut String) {
    if kantu_cmd.Value.is_empty() {
        let cmd_string = format!("\n\t.{}('{}')", kantu_cmd.Command, kantu_cmd.Target);
        nightwatch_test.push_str(&cmd_string);
    } else {
        let cmd_string = format!("\n\t.{}('{}', '{}')", kantu_cmd.Command, kantu_cmd.Target, kantu_cmd.Value);
        nightwatch_test.push_str(&cmd_string);
    }
}

pub fn convert_kantu_cmd(
    kantu_cmd: &mut KantuCommand,
    nightwatch_test: &mut String,
    dict: &Dictionary,
) {
    // Runs through each entry in dict.toml and compares it against the to-be-converted Kantu command.
    // If there's a match, replace the Kantu command's name with the matching NW command's string.
    for i in 0..dict.entries.len() {
        if kantu_cmd.Command == dict.entries[i].k_name.to_string() {
            kantu_cmd.Command = dict.entries[i].nw_name.to_string();
            break;
        }
    }

    process_xpath(kantu_cmd);
    write_nwatch_cmd(kantu_cmd, nightwatch_test);
}

// Kantu uses unconventional formatting for its XPath statements.
// These statements are either standard XPath statements with 'xpath=' appended
// at the starting point, or a unique "shorthand" format that is only understood by Kantu.
// This function reformats those statments to be readable by Nightwatch.
pub fn process_xpath(kantu_cmd: &mut KantuCommand) {
    // If a Kantu XPath statement is found to be a shorthand, run a separate conversion method.
    fn process_shorthand_xpath(kantu_cmd: &mut KantuCommand, mut separator: usize) {
        let key = &kantu_cmd.Target[..separator];
        separator += 1;
        let value = &kantu_cmd.Target[separator..];

        match key {
            "link" => {
                let append = format!("//a[text()=\"{}\"]", value);
                kantu_cmd.Target = append;
            }
            _ => {
                let append = format!("//*[@{}=\"{}\"]", key, value);
                kantu_cmd.Target = append;
            }
        }
    }

    if kantu_cmd.Target.find('=') != None {
        let separator = kantu_cmd.Target.find('=').unwrap();
        match &kantu_cmd.Target[..separator] {
            "xpath" => kantu_cmd.Target.replace_range(0..=separator, ""),
            _ => process_shorthand_xpath(kantu_cmd, separator),
        }
    }
}
