# Inkantation v0.3

Inkantation is a small CLI program that converts Kantu tests' source JSON files into ready-to-use Nightwatch tests.

While it is quite incomplete, Inkantation currently has support for the following:
- Some of Kantu's most common commands and assertions (`click`, `type`, `open`, etc.)
- Converting a single test file or a directory of test files
- XPath selectors
